# READ ME #

## XoCFDNS classes ##

The XoCFDNS (classes) are two classes for obtaining the DNS lookups using the [Cloudflare]( https://cloudflare.com ) system.


### testing ###
currently there is minimal XojoUnit testing.

### optimization ###
currently the methods are not optimized.  All methods are using the first try to complete the method over using the best method.  Optimizations will come in the future.

### sample code ###
there is a sample desktop application will do basic DNS lookups.

### version history ###
* 2018-04-25 - initial commit.
