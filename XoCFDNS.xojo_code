#tag Class
Protected Class XoCFDNS
Inherits Xojo.Net.HTTPSocket
	#tag Event
		Sub Error(err as RuntimeException)
		  self.mIsError   = true
		  self.merrorCode = cstr( err.ErrorNumber ) + EndOfLine + err.Message + EndOfLine + err.Reason + EndOfLine + join( err.Stack, EndOfLine )
		  
		  #IF DebugBuild THEN System.DebugLog self.errorCode
		End Sub
	#tag EndEvent

	#tag Event
		Sub PageReceived(URL as Text, HTTPStatus as Integer, Content as xojo.Core.MemoryBlock)
		  self.parse_PageReceivedEvent( URL,HTTPStatus,Content )
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor
		  
		  dim DNSresponses() as DNSitem
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function convertToMemorBlock(m as Xojo.Core.MemoryBlock) As MemoryBlock
		  dim mbTemp   as MemoryBlock = m.data
		  dim mbRetVal as new MemoryBlock( m.size )
		  mbRetVal.StringValue( 0,mbRetVal.Size ) = mbTemp.StringValue( 0, mbRetVal.size )
		  mbTemp = NIL
		  return mbRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub fetch(name as string, type as integer)
		  #IF DebugBuild THEN System.DebugLog "CloudflareDNS.fetch( " + name + ", " + cstr( type ) + " ) started"
		  self.fetch( name, XoCFDNS.typeIntegerToName( type ) )
		  #IF DebugBuild THEN System.DebugLog "CloudflareDNS.fetch( " + name + ", " + cstr( type ) + " ) ended"
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub fetch(name as string, type as string = "A")
		  self.reset
		  #IF DebugBuild THEN System.DebugLog "CloudflareDNS.fetch( " + name + ", " + type + " ) started"
		  if name.Len < 1 then return 
		  if type.len < 1 then return 
		  if self.ignoreValidityChecks AND NOT self.validateType( type ) then 
		    self.mIsError   = true
		    self.merrorCode = "-1"
		    self.mstatus    = self.kErrorMessage_neg1
		    return
		  end if
		  
		  self.URL = self.kURL_base + "&" + self.kURL_name + name + "&" + self.kURL_type + type
		  #IF DebugBuild THEN System.DebugLog "CloudflareDNS.fetch( " + name + ", " + type + " ) " + self.URL
		  
		  #IF DebugBuild THEN System.DebugLog "CloudflareDNS.fetch( " + name + ", " + type + " ) GET"
		  
		  Send( "GET",self.URL.ToText )
		  #IF DebugBuild THEN System.DebugLog "CloudflareDNS.fetch( " + name + ", " + type + " ) ended"
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub logDetails(returnedData as string, httpstatus as integer)
		  dim dateNow as new date
		  dim tos     as TextOutputStream
		  dateNow.GMTOffset = 0
		  tos = TextOutputStream.Append( SpecialFolder.Desktop.Child( "cloudflaredns.txt" ) )
		  
		  tos.Write dateNow.SQLDateTime + " :: " + "URL: " + self.URL + EndOfLine.UNIX +_
		  dateNow.SQLDateTime + " :: " + "Status Code: " + cstr( httpstatus ) + EndOfLine.UNIX +_
		  dateNow.SQLDateTime + " :: " + "Returned Data: " + returnedData + EndOfLine.UNIX +_
		  "****************************************" + EndOfLine.UNIX
		  tos.flush
		  tos.close
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Attributes( Deprecated ) Private Sub parse_PageReceivedEvent(URL as text, HTTPStatus as integer, Content as xojo.core.memoryblock)
		  #IF DebugBuild THEN System.DebugLog cstr( HTTPStatus )
		  #IF DebugBuild THEN System.DebugLog URL
		  dim strReturnedData as string = Xojo.Core.TextEncoding.UTF8.ConvertDataToText( Content ) 
		  if HTTPStatus = 200 then 
		    // not parsing the return here, just raw return.
		    self.mstatus         = cstr( HTTPStatus)
		    self.rawReturnedData = strReturnedData
		    self.processJSON( strReturnedData )
		    #IF DebugBuild THEN self.logDetails( strReturnedData, HTTPStatus )
		  elseif HTTPStatus = 400 then
		    self.mstatus         = self.kErrorMessage_400
		    self.mIsError        = true
		    self.merrorCode      = "400"
		    self.rawReturnedData = strReturnedData
		    #IF DebugBuild THEN self.logDetails( strReturnedData, HTTPStatus )
		  elseif HTTPStatus = 413 then
		    self.mstatus         = self.kErrorMessage_413
		    self.mIsError        = true
		    self.merrorCode      = "413"
		    self.rawReturnedData = strReturnedData
		    #IF DebugBuild THEN self.logDetails( strReturnedData, HTTPStatus )
		  elseif HTTPStatus = 415 then
		    self.mstatus         = self.kErrorMessage_415
		    self.mIsError        = true
		    self.merrorCode      = "415"
		    self.rawReturnedData = strReturnedData
		    #IF DebugBuild THEN self.logDetails( strReturnedData, HTTPStatus )
		  elseif HTTPStatus = 504 then
		    self.mstatus         = self.kErrorMessage_504
		    self.mIsError        = true
		    self.merrorCode      = "504"
		    self.rawReturnedData = strReturnedData
		    #IF DebugBuild THEN self.logDetails( strReturnedData, HTTPStatus )
		  else 
		    rawReturnedData = ""
		    #If TargetConsole OR TargetWeb THEN
		      System.DebugLog "ERROR: " + cstr( HTTPStatus ) + EndOfLine + "cant finish the transaction"
		    #ELSE
		      MsgBox "ERROR: " + cstr( HTTPStatus ) + EndOfLine + "cant finish the transaction"
		    #Endif
		  end if
		  #IF DebugBuild THEN System.DebugLog rawReturnedData
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub processJSON(jsonString as string)
		  try
		    dim jsonHeaders as new JSONItem( jsonString.ReplaceAll( """""","""" ) )
		    if jsonHeaders.HasName( "AD" )        then self.mAD     = jsonHeaders.Value( "AD" ).BooleanValue
		    if jsonHeaders.HasName( "CD" )        then self.mCD     = jsonHeaders.Value( "CD" ).BooleanValue
		    if jsonHeaders.HasName( "RA" )        then self.mRA     = jsonHeaders.Value( "RA" ).BooleanValue
		    if jsonHeaders.HasName( "RD" )        then self.mRD     = jsonHeaders.Value( "RD" ).BooleanValue
		    if jsonHeaders.HasName( "TC" )        then self.mTC     = jsonHeaders.Value( "TC" ).BooleanValue
		    if jsonHeaders.HasName( "Answer" )    then 
		      self.mrawAnswer = jsonHeaders.Child( "Answer" ).ToString
		      if jsonHeaders.Child( "Answer" ).IsArray then
		        dim intCounter as integer = 0
		        while intCounter < jsonHeaders.Child( "Answer" ).Count
		          dim strName as string  = IF( jsonHeaders.Child( "Answer" ).Child( intCounter ).HasName( "name" ), jsonHeaders.Child( "Answer" ).Child( intCounter ).Value( "name" ).StringValue, "" )
		          dim strData as string  = IF( jsonHeaders.Child( "Answer" ).Child( intCounter ).HasName( "data" ), jsonHeaders.Child( "Answer" ).Child( intCounter ).Value( "data" ).StringValue, "" )
		          dim intTTL as integer  = IF( jsonHeaders.Child( "Answer" ).Child( intCounter ).HasName( "TTL" ),  jsonHeaders.Child( "Answer" ).Child( intCounter ).Value( "TTL" ).IntegerValue, -1 )
		          dim intType as integer = IF( jsonHeaders.Child( "Answer" ).Child( intCounter ).HasName( "type" ), jsonHeaders.Child( "Answer" ).Child( intCounter ).Value( "type" ).IntegerValue,-1 )
		          dim DNSi as new DNSitem( strName,strData,intType,intTTL )
		          self.DNSresponses.Append DNSi
		          intCounter = intCounter + 1
		        wend
		      else
		        System.DebugLog "NOT ARRAY: " + jsonHeaders.Child( "Answer" ).ToString
		      end if
		      
		    elseif jsonHeaders.HasName( "Authority" ) then 
		      self.mrawAnswer = jsonHeaders.Child( "Authority" ).ToString
		      System.DebugLog "Authority: " + jsonHeaders.Child( "Authority" ).ToString
		      if jsonHeaders.Child( "Authority" ).IsArray then
		        dim intCounter as integer = 0
		        while intCounter < jsonHeaders.Child( "Authority" ).Count
		          dim strName as string  = IF( jsonHeaders.Child( "Authority" ).Child( intCounter ).HasName( "name" ), jsonHeaders.Child( "Authority" ).Child( intCounter ).Value( "name" ).StringValue, "" )
		          dim strData as string  = IF( jsonHeaders.Child( "Authority" ).Child( intCounter ).HasName( "data" ), jsonHeaders.Child( "Authority" ).Child( intCounter ).Value( "data" ).StringValue, "" )
		          dim intTTL as integer  = IF( jsonHeaders.Child( "Authority" ).Child( intCounter ).HasName( "TTL" ),  jsonHeaders.Child( "Authority" ).Child( intCounter ).Value( "TTL" ).IntegerValue, -1 )
		          dim intType as integer = IF( jsonHeaders.Child( "Authority" ).Child( intCounter ).HasName( "type" ), jsonHeaders.Child( "Authority" ).Child( intCounter ).Value( "type" ).IntegerValue,-1 )
		          dim DNSi as new DNSitem( strName,strData,intType,intTTL )
		          self.DNSresponses.Append DNSi
		          intCounter = intCounter + 1
		        wend
		      else
		        System.DebugLog "NOT ARRAY: " + jsonHeaders.Child( "Authority" ).ToString
		      end if
		    end
		  catch
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub reset()
		  self.URL             = ""
		  self.rawReturnedData = ""
		  self.mIsError        = false
		  self.merrorCode      = ""
		  self.mstatus         = ""
		  self.mAD             = false
		  self.mCD             = false
		  self.mRA             = false
		  self.mRD             = false
		  self.mTC             = false
		  self.mrawAnswer      = ""
		  redim self.DNSresponses(-1)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function typeIntegerToName(type as integer) As string
		  select case type
		  case TypeA
		    return "A"
		  case TypeNS
		    return "NS"
		  case TypeCNAME
		    return "CNAME"
		  case TypeSOA
		    return "SOA"
		  case TypePTR
		    return "PTR"
		  case TypeMX
		    return "MX"
		  case TypeTXT
		    return "TXT"
		  case TypeSIG
		    return "SIG"
		  case TypeKEY
		    return "KEY"
		  case TypeAAAA
		    return "AAAA"
		  case TypeOPENPGPKEY
		    return "OPENPGPKEY"
		  else
		    return "???? " + cstr( type )
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function validateType(type as string) As boolean
		  dim types() as string = self.kValidTypes.Uppercase.Split( ";" )
		  
		  if types.IndexOf( type.Uppercase ) >= 0 then return true
		  
		  return false
		End Function
	#tag EndMethod


	#tag Note, Name = List of TYPES
		
		1  A
		2  NS
		5  CNAME
		6  SOA
		12 PTR
		15 MX
		16 TXT
		24 SIG
		25 KEY
		28 AAAA
		61 OPENPGPKEY
		
		need to add more....
		
	#tag EndNote

	#tag Note, Name = XojoUNIT test
		
		need to add XojoUnit Tests
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAD
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'mAD = value
			  
			  // READ ONLY variable
			End Set
		#tag EndSetter
		AD As boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCD
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'mCD = value
			  
			  // READ ONLY variable
			End Set
		#tag EndSetter
		CD As boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		DNSresponses() As DNSitem
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return merrorCode
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'merrorCode = value
			  
			  // READY ONLY VARIABLE
			End Set
		#tag EndSetter
		errorCode As string
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		ignoreValidityChecks As boolean = false
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mIsError
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'mIsError = value
			  
			  // READ ONLY VARIABLE
			End Set
		#tag EndSetter
		IsError As boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAD As boolean = false
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCD As boolean = false
	#tag EndProperty

	#tag Property, Flags = &h21
		Private merrorCode As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mIsError As boolean = false
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRA As boolean = false
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mrawAnswer As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRD As boolean = false
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mstatus As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTC As boolean = false
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mRA
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'mRA = value
			  
			  // READ ONLY variable
			End Set
		#tag EndSetter
		RA As boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mrawAnswer
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'mrawAnswer = value
			  
			  // READ ONLY variable
			End Set
		#tag EndSetter
		rawAnswer As string
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		rawReturnedData As string
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mRD
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'mRD = value
			  
			  // READ ONLY variable
			End Set
		#tag EndSetter
		RD As boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mstatus
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'mstatus = value
			  
			  // READ ONLY VARIABLE
			End Set
		#tag EndSetter
		status As string
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTC
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'mTC = value
			  
			  // READ ONLY variable
			End Set
		#tag EndSetter
		TC As boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private URL As string
	#tag EndProperty


	#tag Constant, Name = kErrorMessage_400, Type = String, Dynamic = False, Default = \"DNS query not specied or too small.", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kErrorMessage_413, Type = String, Dynamic = False, Default = \"DNS query is larger than maximum allowed DNS message size.", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kErrorMessage_415, Type = String, Dynamic = False, Default = \"Unsupported content type.", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kErrorMessage_504, Type = String, Dynamic = False, Default = \"Resolver timeout while waiting for the query response.", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kErrorMessage_neg1, Type = String, Dynamic = False, Default = \"Invalid DNS Type.", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kURL_base, Type = String, Dynamic = False, Default = \"https://cloudflare-dns.com/dns-query\?ct\x3Dapplication/dns-json", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kURL_name, Type = String, Dynamic = False, Default = \"name\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kURL_type, Type = String, Dynamic = False, Default = \"type\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kValidTypes, Type = String, Dynamic = False, Default = \"A;NS;CNAME;SOA;MX;TXT;SIG;KEY;PTR;AAAA;OPENPGPKEY", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kVersion, Type = String, Dynamic = False, Default = \"20180506", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TypeA, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TypeAAAA, Type = Double, Dynamic = False, Default = \"28", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TypeCNAME, Type = Double, Dynamic = False, Default = \"5", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TypeKEY, Type = Double, Dynamic = False, Default = \"25", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TypeMX, Type = Double, Dynamic = False, Default = \"15", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TypeNS, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TypeOPENPGPKEY, Type = Double, Dynamic = False, Default = \"61", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TypePTR, Type = Double, Dynamic = False, Default = \"12", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TypeSIG, Type = Double, Dynamic = False, Default = \"24", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TypeSOA, Type = Double, Dynamic = False, Default = \"6", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TypeTXT, Type = Double, Dynamic = False, Default = \"16", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AD"
			Group="Behavior"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CD"
			Group="Behavior"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="errorCode"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ignoreValidityChecks"
			Group="Behavior"
			InitialValue="false"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsError"
			Group="Behavior"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RA"
			Group="Behavior"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rawAnswer"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rawReturnedData"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RD"
			Group="Behavior"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="status"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TC"
			Group="Behavior"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ValidateCertificates"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
