#tag Class
Protected Class App
Inherits Application
	#tag Event
		Sub Open()
		  
		  
		  
		  
		  // code stolen from XojoUnit console example app and modified.
		  
		  mController = New ConsoleTestController
		  mController.LoadTestGroups
		  mController.Start
		  While mController.IsRunning
		    App.DoEvents
		  Wend
		  Dim outputFile As FolderItem
		  outputFile = OutputResults
		  
		  // end of stolen code
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Function OutputResults() As FolderItem
		  // code stolen from XojoUnit console example app and modified.
		  
		  Const kIndent = "   "
		  Const kFailIndent = " * "
		  
		  Dim outputFile As FolderItem
		  outputFile = GetFolderItem( "CloudfareDNS-exmaple-unit-tests.txt" )
		  
		  
		  Dim output As TextOutputStream = TextOutputStream.Create(outputFile)
		  
		  PrintSummary(output, mController)
		  
		  For Each tg As TestGroup In mController.TestGroups
		    output.WriteLine("")
		    output.WriteLine(tg.Name)
		    
		    For Each tr As TestResult In tg.Results
		      Dim useIndent As String = If(tr.Result = tr.Failed, kFailIndent, kIndent)
		      Dim outLine As String = useIndent + tr.TestName + ": " + tr.Result + " (" + Format(tr.Duration, "#,###.0000000") + "s)"
		      output.WriteLine(outLine)
		      
		      Dim msg As String = tr.Message
		      If msg <> "" Then
		        msg = ReplaceLineEndings(msg, EndOfLine)
		        Dim msgs() As String = msg.Split(EndOfLine)
		        
		        For i As Integer = 0 To msgs.Ubound
		          msg = msgs(i)
		          outLine  = kIndent + kIndent + msg
		          output.WriteLine(outLine)
		        Next i
		      End If
		    Next
		  Next
		  
		  output.Close
		  
		  Return outputFile
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub PrintSummary(output As Writeable, controller As TestController)
		  // code stolen from XojoUnit console example app and modified.
		  
		  Dim now As New Date
		  
		  Dim allTestCount As Integer = controller.AllTestCount
		  Dim runTestCount As Integer = controller.RunTestCount
		  
		  output.Write "Start: " + now.ShortDate + " " + now.ShortTime + EndOfLine
		  output.Write "Duration: " + Format(controller.Duration, "#,###.0000000") + "s" + EndOfLine
		  output.Write "Groups: " + Format(controller.RunGroupCount, "#,0") + EndOfLine
		  output.Write "Total: " + Str(runTestCount) + If(allTestCount <> runTestCount, " of " + Str(allTestCount), "") + " tests were run" + EndOfLine
		  output.Write "Passed: " + Str(controller.PassedCount) + _
		  If(runTestCount = 0, "", " (" + Format((controller.PassedCount / runTestCount) * 100, "##.00") + "%)") + EndOfLine
		  output.Write "Failed: " + Str(controller.FailedCount) + _
		  If(runTestCount = 0, "", " (" + Format((controller.FailedCount / runTestCount) * 100, "##.00") + "%)") + EndOfLine
		  output.Write "Skipped: " + Str(controller.SkippedCount) + EndOfLine
		  output.Write "Not Implemented: " + Str(controller.NotImplementedCount) + EndOfLine
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private mController As TestController
	#tag EndProperty


	#tag Constant, Name = kEditClear, Type = String, Dynamic = False, Default = \"&Delete", Scope = Public
		#Tag Instance, Platform = Windows, Language = Default, Definition  = \"&Delete"
		#Tag Instance, Platform = Linux, Language = Default, Definition  = \"&Delete"
	#tag EndConstant

	#tag Constant, Name = kFileQuit, Type = String, Dynamic = False, Default = \"&Quit", Scope = Public
		#Tag Instance, Platform = Windows, Language = Default, Definition  = \"E&xit"
	#tag EndConstant

	#tag Constant, Name = kFileQuitShortcut, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Mac OS, Language = Default, Definition  = \"Cmd+Q"
		#Tag Instance, Platform = Linux, Language = Default, Definition  = \"Ctrl+Q"
	#tag EndConstant


	#tag ViewBehavior
	#tag EndViewBehavior
End Class
#tag EndClass
