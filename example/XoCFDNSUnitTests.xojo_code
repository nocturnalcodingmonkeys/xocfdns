#tag Class
Protected Class XoCFDNSUnitTests
Inherits TestGroup
	#tag Method, Flags = &h0
		Sub kVersionTest()
		  Dim strVersion As string = "20180424"
		  
		  Assert.AreEqual( strVersion,XoCFDNS.kVersion )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeAAAATest()
		  Dim intType As integer = 28
		  
		  Assert.AreEqual( intType,XoCFDNS.TypeAAAA )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeATest()
		  Dim intType As integer = 1
		  
		  Assert.AreEqual( intType,XoCFDNS.TypeA ) 
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeCNAMETest()
		  Dim intType As integer = 5
		  
		  Assert.AreEqual( intType,XoCFDNS.TypeCNAME )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextAAAATest()
		  Dim strType As string = "AAAA"
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( 28 ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextAAAA_2Test()
		  Dim strType As string = "AAAA"
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( XoCFDNS.TypeAAAA ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextATest()
		  Dim strType As string = "A" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( 1 ) ) 
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextA_2Test()
		  Dim strType As string = "A" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( XoCFDNS.TypeA ) ) 
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextCNAMETest()
		  Dim strType As string = "CNAME" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( 5 ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextCNAME_2Test()
		  Dim strType As string = "CNAME" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( XoCFDNS.TypeCNAME ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextKEYTest()
		  Dim strType As string = "KEY" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( 25 ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextKEY_2Test()
		  Dim strType As string = "KEY" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( XoCFDNS.TypeKEY ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextMXTest()
		  Dim strType As string = "MX" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( 15 ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextMX_2Test()
		  Dim strType As string = "MX" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( XoCFDNS.TypeMX ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextNSTest()
		  Dim strType As string = "NS" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( 2 ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextNS_2Test()
		  Dim strType As string = "NS" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( XoCFDNS.TypeNS ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextOPENPGPKEYTest()
		  Dim strType As string = "OPENPGPKEY" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( 61 ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextOPENPGPKEY_2Test()
		  Dim strType As string = "OPENPGPKEY" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( XoCFDNS.TypeOPENPGPKEY ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextPTRTest()
		  Dim strType As string = "PTR" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( 12 ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextPTR_2Test()
		  Dim strType As string = "PTR" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( XoCFDNS.TypePTR ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextSIGTest()
		  Dim strType As string = "SIG"
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( 24 ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextSIG_2Test()
		  Dim strType As string = "SIG"
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( XoCFDNS.TypeSIG ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextSOATest()
		  Dim strType As string = "SOA" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( 6 ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextSOA_2Test()
		  Dim strType As string = "SOA" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( XoCFDNS.TypeSOA ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextTXTTest()
		  Dim strType As string = "TXT"
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( 16 ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeIntegerToTextTXT_2Test()
		  Dim strType As string = "TXT" 
		  
		  Assert.AreEqual( strType,XoCFDNS.typeIntegerToName( XoCFDNS.TypeTXT ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeKEYTest()
		  Dim intType As integer = 25
		  
		  Assert.AreEqual( intType,XoCFDNS.TypeKEY )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeMXTest()
		  Dim intType As integer = 15
		  
		  Assert.AreEqual( intType,XoCFDNS.TypeMX )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeNSTest()
		  Dim intType As integer = 2
		  
		  Assert.AreEqual( intType,XoCFDNS.TypeNS )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeOPENPGPKEYTest()
		  Dim intType As integer = 61
		  
		  Assert.AreEqual( intType,XoCFDNS.TypeOPENPGPKEY )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypePTRTest()
		  Dim intType As integer = 12
		  
		  Assert.AreEqual( intType,XoCFDNS.TypePTR )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeSIGTest()
		  Dim intType As integer = 24
		  
		  Assert.AreEqual( intType,XoCFDNS.TypeSIG )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeSOATest()
		  Dim intType As integer = 6
		  
		  Assert.AreEqual( intType,XoCFDNS.TypeSOA )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TypeTXTTest()
		  Dim intType As integer = 16
		  
		  Assert.AreEqual( intType,XoCFDNS.TypeTXT )
		End Sub
	#tag EndMethod


	#tag Note, Name = Tests
		I have created a few simple XojoUnit tests to make sure things work.
		We need to make more are.
		
	#tag EndNote


	#tag ViewBehavior
		#tag ViewProperty
			Name="Duration"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FailedTestCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IncludeGroup"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsRunning"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="NotImplementedCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PassedTestCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RunTestCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SkippedTestCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StopTestOnFail"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TestCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
